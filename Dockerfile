FROM openjdk:11.0.7-jre-slim-buster
#
ENV APP_PTH="/tin-service/app.jar" \
    LOGGING_FILE_PATH="/tin-service/logs" \
    LOGGING_FILE_NAME="/tin-service/logs/app.log"
#
COPY ./build/libs/*.jar ${APP_PTH}
#
ENTRYPOINT java -Xmx4G -jar ${APP_PTH}
#