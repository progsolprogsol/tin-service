create table tin.country
(
    id                  bigserial primary key,
    name                varchar(50) not null unique,
    code                varchar(50) not null unique,
    polish_translation  varchar(50) not null,
    english_translation varchar(50) not null
);

INSERT INTO tin.country(name, code, polish_translation, english_translation)
VALUES ('AUSTRIA', 'AT', 'Austria', 'Austria'),
       ('BELGIUM', 'BE', 'Belgia', 'Belgium'),
       ('BULGARIA', 'BG', 'Bułgaria', 'Bulgaria'),
       ('CYPRUS', 'CY', 'Cypr', 'Cyprus'),
       ('CZECH_REPUBLIC', 'CZ', 'Czechy', 'Czech Republic'),
       ('GERMANY', 'DE', 'Niemcy', 'Germany'),
       ('DENMARK', 'DK', 'Dania', 'Denmark'),
       ('ESTONIA', 'EE', 'Estonia', 'Estonia'),
       ('GREECE', 'EL', 'Grecja', 'Greece'),
       ('SPAIN', 'ES', 'Hiszpania', 'Spain'),
       ('FINLAND', 'FI', 'Finlandia', 'Finland'),
       ('FRANCE', 'FR', 'Francja', 'France'),
       ('GRATE_BRITAIN', 'GB', 'Wielka Brytania', 'Grate Britain'),
       ('CROATIA', 'HR', 'Chorwacja', 'Croatia'),
       ('HUNGARY', 'HU', 'Węgry', 'Hungary'),
       ('IRELAND', 'IE', 'Irlandia', 'Ireland'),
       ('ITALY', 'IT', 'Włochy', 'Italy'),
       ('LITHUANIA', 'LT', 'Litwa', 'Lithuania'),
       ('LUXEMBOURG', 'LU', 'Luksemburg', 'Luxembourg'),
       ('LATVIA', 'LV', 'Łotwa', 'Latvia'),
       ('MALTA', 'MT', 'Malta', 'Malta'),
       ('NETHERLANDS', 'NL', 'Holandia', 'Netherlands'),
       ('POLAND', 'PL', 'Polska', 'Poland'),
       ('PORTUGAL', 'PT', 'Portugalia', 'Portugal'),
       ('ROMANIA', 'RO', 'Rumunia', 'Romania'),
       ('SWEDEN', 'SE', 'Szwecja', 'Sweden'),
       ('SLOVENIA', 'SI', 'Słowenia', 'Slovenia'),
       ('SLOVAKIA', 'SK', 'Słowacja', 'Slovakia')
ON CONFLICT DO NOTHING;