create table tin.company_vies
(
    id              bigserial primary key,
    last_refresh    timestamp,
    country_id      bigint references tin.country (id),
    vat_number      varchar(15),
    company_name    varchar(2000),
    company_address varchar(2000)
);
create unique index on tin.company_vies (country_id, vat_number);