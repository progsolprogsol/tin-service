create table tin.voivodeship
(
    id               bigserial primary key,
    voivodeship_name varchar(200) not null
);
create table tin.company_gus
(
    id                        bigserial primary key,
    last_refresh              timestamp not null,
    regon                     varchar(14),
    nip                       varchar(10),
    nip_state                 varchar(12),
    company_name              varchar(2000),
    voivodeship_id            bigint    not null references tin.voivodeship (id),
    county                    varchar(200),
    commune                   varchar(200),
    place                     varchar(200),
    zip_code                  varchar(12),
    place_of_post_office      varchar(200),
    street                    varchar(200),
    property_number           varchar(20),
    apartment_number          varchar(10),
    company_type              varchar(2),
    silos_id                  int,
    business_termination_date date
);
create index on tin.company_gus (nip);