package pl.progsol.tinservice.country;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CountryMapper {

    @Mappings({
            @Mapping(target = "name", source = "englishTranslation")
    })
    CountryDto countryToCountryDto(Country country);
}
