package pl.progsol.tinservice.country;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Country")
@Getter
@Setter
public class CountryDto {
    private String code;
    private String name;
}
