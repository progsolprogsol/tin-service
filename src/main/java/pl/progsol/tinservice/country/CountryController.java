package pl.progsol.tinservice.country;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/countries")
@RequiredArgsConstructor
public class CountryController {
    private final CountryService countryService;

    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping
    public List<CountryDto> getAllCountries() {
        return countryService.getAllCompanies();
    }

    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
    })
    @GetMapping("/{countryCode}")
    public CountryDto getCountryByCode(
            @PathVariable String countryCode
    ) {
        return countryService.getCompanyByCode(countryCode);
    }
}
