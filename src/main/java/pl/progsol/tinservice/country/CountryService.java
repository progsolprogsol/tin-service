package pl.progsol.tinservice.country;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryService {
    private final CountryRepository countryRepository;
    private final CountryMapper countryMapper;

    public List<CountryDto> getAllCompanies() {
        return countryRepository.findAll().stream()
                .map(countryMapper::countryToCountryDto)
                .sorted(Comparator.comparing(CountryDto::getCode))
                .collect(Collectors.toList());
    }

    public CountryDto getCompanyByCode(String countryCode) {
        Country country = countryRepository.findByCode(countryCode);
        return countryMapper.countryToCountryDto(country);
    }
}
