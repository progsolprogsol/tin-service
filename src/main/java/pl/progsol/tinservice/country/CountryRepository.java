package pl.progsol.tinservice.country;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCode(String code);
    boolean existsByCode(String code);
}
