package pl.progsol.tinservice.companyvies;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.progsol.euapivies.ViesCompany;

@Mapper(componentModel = "spring")
public interface ViesCompanyMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "lastRefresh", ignore = true),
            @Mapping(target = "country", ignore = true),
            @Mapping(target = "companyName", source = "name"),
            @Mapping(target = "companyAddress", source = "address")
    })
    CompanyVies viesCompanyToCompanyVies(ViesCompany viesCompany);
}
