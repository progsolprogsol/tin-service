package pl.progsol.tinservice.companyvies;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.progsol.tinservice.country.Country;

import java.util.Optional;

public interface CompanyViesRepository extends JpaRepository<CompanyVies, Long> {
    Optional<CompanyVies> findByCountryCodeAndVatNumber(String countryCode, String vatNumber);
    void deleteAllByCountryAndVatNumber(Country country, String vatNumber);
}
