package pl.progsol.tinservice.companyvies;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.progsol.euapivies.EuApiViesService;
import pl.progsol.euapivies.ViesCountry;
import pl.progsol.tinservice.country.Country;
import pl.progsol.tinservice.country.CountryRepository;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyViesService {

    @Value("${tin-service.maximum-date-age-in-millis}")
    private Long maxDateAgeInMillis;

    private final CompanyViesRepository companyViesRepository;
    private final CountryRepository countryRepository;
    private final EuApiViesService euApiViesService;
    private final ViesCompanyMapper viesCompanyMapper;

    @Transactional
    public Optional<CompanyVies> getCompanyVies(String countryCode, String vatNumber) {
        Optional<CompanyVies> oCompanyVies = companyViesRepository.findByCountryCodeAndVatNumber(countryCode, vatNumber);
        if (oCompanyVies.isEmpty()) {
            return createAndSaveNewCompany(countryCode, vatNumber);
        } else if (dataIsTooOld(oCompanyVies.get())) {
            return refreshAndSaveCompany(oCompanyVies.get());
        } else {
            return oCompanyVies;
        }
    }

    private Optional<CompanyVies> createAndSaveNewCompany(String countryCode, String vatNumber) {
        Country country = Objects.requireNonNull(countryRepository.findByCode(countryCode));
        Optional<CompanyVies> oCompanyVies = getCompanyFromViesApi(country, vatNumber);
        oCompanyVies.ifPresent(companyViesRepository::save);
        return oCompanyVies;
    }

    private boolean dataIsTooOld(CompanyVies companyVies) {
        return Duration.between(companyVies.getLastRefresh(), LocalDateTime.now()).toMillis() > maxDateAgeInMillis;
    }

    private Optional<CompanyVies> refreshAndSaveCompany(CompanyVies oldCompany) {
        Optional<CompanyVies> oRefreshedCompany = getCompanyFromViesApi(oldCompany.getCountry(), oldCompany.getVatNumber());
        oRefreshedCompany.ifPresentOrElse(
                company -> {
                    company.setId(oldCompany.getId());
                    companyViesRepository.save(company);
                },
                () -> companyViesRepository.deleteAllByCountryAndVatNumber(oldCompany.getCountry(), oldCompany.getVatNumber())
        );
        return oRefreshedCompany;
    }

    private Optional<CompanyVies> getCompanyFromViesApi(Country country, String vatNumber) {
        ViesCountry viesCountry = ViesCountry.valueOf(country.getName());
        return euApiViesService.getViesCompany(viesCountry, vatNumber)
                .map(viesCompany -> {
                    CompanyVies companyVies = viesCompanyMapper.viesCompanyToCompanyVies(viesCompany);
                    companyVies.setLastRefresh(LocalDateTime.now());
                    companyVies.setCountry(country);
                    return companyVies;
                });
    }
}
