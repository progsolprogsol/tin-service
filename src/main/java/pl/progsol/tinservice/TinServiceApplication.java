package pl.progsol.tinservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TinServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(TinServiceApplication.class, args);
    }
}
