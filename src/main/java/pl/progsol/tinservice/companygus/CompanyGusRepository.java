package pl.progsol.tinservice.companygus;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanyGusRepository extends JpaRepository<CompanyGus, Long> {
    List<CompanyGus> findAllByNip(String nip);
}
