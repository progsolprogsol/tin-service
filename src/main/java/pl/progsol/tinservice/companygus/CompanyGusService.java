package pl.progsol.tinservice.companygus;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.progsol.gusapiregon.GusApiRegonService;
import pl.progsol.gusapiregon.xmlmodel.CompanyBasicInfo;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CompanyGusService {

    @Value("${tin-service.maximum-date-age-in-millis}")
    private Long maxDateAgeInMillis;

    private final CompanyGusRepository companyGusRepository;
    private final VoivodeshipRepository voivodeshipRepository;
    private final GusApiRegonService gusApiRegonService;
    private final CompanyBasicInfoMapper companyBasicInfoMapper;

    @Transactional
    public List<CompanyGus> getCompanyGusList(String nip) {
        List<CompanyGus> companyGusList = companyGusRepository.findAllByNip(nip);
        if (companyGusList.isEmpty()) {
            return createAndSaveNewCompany(nip);
        } else if (dataIsTooOld(companyGusList)) {
            return refreshAndSaveCompany(nip, companyGusList);
        } else {
            return companyGusList;
        }
    }

    private List<CompanyGus> createAndSaveNewCompany(String nip) {
        List<CompanyGus> companyGusList = getCompanyFromGusApi(nip);
        if (!companyGusList.isEmpty()) {
            companyGusRepository.saveAll(companyGusList);
        }
        return companyGusList;
    }

    private boolean dataIsTooOld(List<CompanyGus> companyGusList) {
        List<LocalDateTime> lastRefreshes = companyGusList.stream()
                .map(CompanyGus::getLastRefresh)
                .sorted(LocalDateTime::compareTo)
                .collect(Collectors.toList());
        return Duration.between(lastRefreshes.get(0), LocalDateTime.now()).toMillis() > maxDateAgeInMillis;
    }

    private List<CompanyGus> refreshAndSaveCompany(String nip, List<CompanyGus> oldCompanies) {
        List<CompanyGus> refreshedCompanies = getCompanyFromGusApi(nip);
        companyGusRepository.deleteInBatch(oldCompanies);
        if (!refreshedCompanies.isEmpty()) {
            companyGusRepository.saveAll(refreshedCompanies);
        }
        return refreshedCompanies;
    }

    private List<CompanyGus> getCompanyFromGusApi(String nip) {
        List<CompanyBasicInfo> companyBasicInfoList = gusApiRegonService.getCompanyBasicInfoListByNip(nip);
        if (companyBasicInfoList.isEmpty()) {
            return Collections.emptyList();
        }
        List<CompanyGus> companyGusList = companyBasicInfoList.stream()
                .map(companyBasicInfoMapper::companyBasicInfoToCompanyGus)
                .collect(Collectors.toList());
        companyGusList.forEach(companyGus -> companyGus.setLastRefresh(LocalDateTime.now()));
        companyGusList.forEach(this::setVoivodeship);
        return companyGusList;
    }

    private void setVoivodeship(CompanyGus companyGus) {
        Voivodeship voivodeship = getOrCreateVoivodeship(companyGus.getVoivodeship().getName());
        companyGus.setVoivodeship(voivodeship);
    }

    private Voivodeship getOrCreateVoivodeship(String voivodeshipName) {
        return voivodeshipRepository.findByName(voivodeshipName).orElseGet(() -> {
            Voivodeship voivodeship = new Voivodeship(voivodeshipName);
            voivodeshipRepository.save(voivodeship);
            return voivodeship;
        });
    }
}
