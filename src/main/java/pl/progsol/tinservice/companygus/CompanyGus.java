package pl.progsol.tinservice.companygus;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "company_gus", schema = "tin")
public class CompanyGus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "last_refresh")
    private LocalDateTime lastRefresh;
    private String regon;
    private String nip;
    @Column(name = "nip_state")
    private String nipState;
    @Column(name = "company_name")
    private String companyName;
    @ManyToOne(optional = false)
    @JoinColumn(name = "voivodeship_id", referencedColumnName = "id")
    private Voivodeship voivodeship;
    private String county;
    private String commune;
    private String place;
    @Column(name = "zip_code")
    private String zipCode;
    @Column(name = "place_of_post_office")
    private String placeOfPostOffice;
    private String street;
    @Column(name = "property_number")
    private String propertyNumber;
    @Column(name = "apartment_number")
    private String apartmentNumber;
    @Column(name = "company_type")
    private String companyType;
    @Column(name = "silos_id")
    private Integer silosId;
    @Column(name = "business_termination_date")
    private LocalDate businessTerminationDate;
}
