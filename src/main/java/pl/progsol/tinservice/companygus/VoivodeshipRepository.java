package pl.progsol.tinservice.companygus;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoivodeshipRepository extends JpaRepository<Voivodeship, Long> {
    Optional<Voivodeship> findByName(String name);
}
