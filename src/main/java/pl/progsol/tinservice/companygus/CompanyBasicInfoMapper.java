package pl.progsol.tinservice.companygus;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.progsol.gusapiregon.xmlmodel.CompanyBasicInfo;

@Mapper(componentModel = "spring")
public interface CompanyBasicInfoMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "lastRefresh", ignore = true),
            @Mapping(target = "nipState", source = "statusNip"),
            @Mapping(target = "companyName", source = "nazwa"),
            @Mapping(target = "voivodeship.name", source = "wojewodztwo"),
            @Mapping(target = "county", source = "powiat"),
            @Mapping(target = "commune", source = "gmina"),
            @Mapping(target = "place", source = "miejscowosc"),
            @Mapping(target = "zipCode", source = "kodPocztowy"),
            @Mapping(target = "placeOfPostOffice", source = "miejscowoscPoczty"),
            @Mapping(target = "street", source = "ulica"),
            @Mapping(target = "propertyNumber", source = "nrNieruchomosci"),
            @Mapping(target = "apartmentNumber", source = "nrLokalu"),
            @Mapping(target = "companyType", source = "typ"),
            @Mapping(target = "silosId", source = "silosId"),
            @Mapping(target = "businessTerminationDate", source = "dataZakonczeniaDzialalnosci")
    })
    CompanyGus companyBasicInfoToCompanyGus(CompanyBasicInfo companyBasicInfo);
}
