package pl.progsol.tinservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.progsol.euapivies.EuApiViesService;

@Configuration
public class ApiViesConfig {
    @Bean
    public EuApiViesService euApiViesService() {
        return new EuApiViesService();
    }
}
