package pl.progsol.tinservice.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import pl.progsol.tinservice.company.CompanyNotFoundException;
import pl.progsol.tinservice.company.inputchecker.WrongCompanyInputDataException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestControllerAdvice
public class ErrorHandler {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @ExceptionHandler(WrongCompanyInputDataException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorResponse handleWrongCompanyInputDataException(Exception e) {
        return toErrorResponse(e);
    }

    @ExceptionHandler({
            CompanyNotFoundException.class,
            NoHandlerFoundException.class
    })
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorResponse handleCompanyNotFoundException(Exception e) {
        return toErrorResponse(e);
    }

    private ErrorResponse toErrorResponse(Exception e) {
        return new ErrorResponse(
                LocalDateTime.now().format(dateTimeFormatter),
                e.getClass().getSimpleName(),
                e.getMessage()
        );
    }
}
