package pl.progsol.tinservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.progsol.gusapiregon.GusApiRegonService;

@Configuration
public class ApiGusConfig {

    @Value("${tin-service.gus-api-user-key}")
    private String gusApiUserKey;

    @Bean
    public GusApiRegonService gusApiRegonService() {
        return new GusApiRegonService(gusApiUserKey);
    }
}
