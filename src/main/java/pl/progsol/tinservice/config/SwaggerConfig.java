package pl.progsol.tinservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {
    @Bean
    public Docket swaggerApi10() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .groupName("tin-api-1.0")
                .select()
                .apis(RequestHandlerSelectors.basePackage("pl.progsol.tinservice"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo10());
    }

    private ApiInfo apiInfo10() {
        return new ApiInfoBuilder()
                .version("1.0")
                .title("Tax Identification Number (TIN) Service")
                .description("Service which aggregate basic information about all Polish and European companies")
                .contact(new Contact(
                        "Daniel Wieteska",
                        "http://progsol.pl",
                        "wieteskadaniel@gmail.com"
                )).build();
    }

    // we need configure Resource Handler because we switch off default mapping
    // (application.yml -> spring.resources.add-mappings: false)
    // we switch them off because we crate global custom error handler -> ErrorHandler.class
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
