package pl.progsol.tinservice.company;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Company {
    private String countryCode;
    private String countryName;
    private String companyTaxIdentificationNumber;
    private String companyName;
    private String companyAddress;
}
