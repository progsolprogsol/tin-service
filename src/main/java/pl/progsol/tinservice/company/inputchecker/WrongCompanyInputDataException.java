package pl.progsol.tinservice.company.inputchecker;

public class WrongCompanyInputDataException extends RuntimeException {

    public WrongCompanyInputDataException(String message) {
        super(message);
    }
}
