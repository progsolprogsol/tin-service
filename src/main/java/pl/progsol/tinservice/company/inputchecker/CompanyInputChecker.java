package pl.progsol.tinservice.company.inputchecker;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import pl.progsol.tinservice.country.CountryRepository;

@Aspect
@Component
@RequiredArgsConstructor
public class CompanyInputChecker {

    private final CountryRepository countryRepository;

    // value means: run before methods annotated with @CompanyCheckInput and which takes two String arguments
    // argNames does not mater, it can be also arg1,arg2
    @Before(
            value = "@annotation(CompanyCheckInput) && args(countryCode,taxIdentificationNumber)",
            argNames = "countryCode,taxIdentificationNumber"
    )
    public void checkInputParameters(String countryCode, String taxIdentificationNumber) {
        checkTaxIdentifierNumber(taxIdentificationNumber);
        checkCountryCode(countryCode);
    }

    private void checkTaxIdentifierNumber(String taxIdentificationNumber) {
        boolean tinCorrect = taxIdentificationNumber.matches("[A-Z0-9]+");
        if (!tinCorrect) {
            throw new WrongCompanyInputDataException(
                    "Tax Identification Number (" + taxIdentificationNumber + ") is incorrect"
            );
        }
    }

    private void checkCountryCode(String countryCode) {
        boolean countryCodeCorrect = countryRepository.existsByCode(countryCode);
        if (!countryCodeCorrect) {
            throw new WrongCompanyInputDataException(
                    "Country Code (" + countryCode + ") is incorrect, visit /countries endpoint to see allowed country codes"
            );
        }
    }
}
