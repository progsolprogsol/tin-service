package pl.progsol.tinservice.company.inputchecker;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
// @Retention states whether the annotation will be available to the JVM at runtime or not.
// By default it is not, so Spring AOP would not be able to see the annotation. This is why it's been reconfigured.
@Retention(RetentionPolicy.RUNTIME)
public @interface CompanyCheckInput {
}
