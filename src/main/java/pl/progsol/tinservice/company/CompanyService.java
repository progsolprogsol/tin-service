package pl.progsol.tinservice.company;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.progsol.tinservice.companygus.CompanyGus;
import pl.progsol.tinservice.companygus.CompanyGusService;
import pl.progsol.tinservice.companyvies.CompanyVies;
import pl.progsol.tinservice.companyvies.CompanyViesService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CompanyService {
    private final CompanyGusService companyGusService;
    private final CompanyViesService companyViesService;

    private final CompanyGusMapper companyGusMapper;
    private final CompanyViesMapper companyViesMapper;

    public List<Company> getCompanies(String countryCode, String taxIdentificationNumber) {
        if (countryCode.equals("PL")) {
            return getPolishCompanies(taxIdentificationNumber);
        } else {
            Company europeanCompany = getEuropeanCompany(countryCode, taxIdentificationNumber);
            return Collections.singletonList(europeanCompany);
        }
    }

    private List<Company> getPolishCompanies(String nip) {
        List<CompanyGus> companyGusList = companyGusService.getCompanyGusList(nip);
        if (companyGusList.isEmpty()) {
            throw new CompanyNotFoundException("Company (PL " + nip + ") not found");
        } else {
            return companyGusList.stream()
                    .map(companyGusMapper::companyGusToCompany)
                    .collect(Collectors.toList());
        }
    }

    private Company getEuropeanCompany(String countryCode, String taxIdentificationNumber) {
        Optional<CompanyVies> oCompanyVies = companyViesService.getCompanyVies(countryCode, taxIdentificationNumber);
        if (oCompanyVies.isEmpty()) {
            throw new CompanyNotFoundException("Company (" + countryCode + " " + taxIdentificationNumber + ") not found");
        } else {
            return companyViesMapper.companyViesToCompany(oCompanyVies.get());
        }
    }
}
