package pl.progsol.tinservice.company;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.progsol.tinservice.company.inputchecker.CompanyCheckInput;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/companies")
public class CompanyController {

    private final CompanyService companyService;

    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Input is correct, but resource not found"),
            @ApiResponse(code = 422, message = "Input (countryCode or tin) is incorrect")
    })
    @CompanyCheckInput
    @GetMapping(
            path = "/{countryCode}/{taxIdentificationNumber}",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public List<Company> getCompany(
            @PathVariable String countryCode,
            @PathVariable String taxIdentificationNumber
    ) {
        return companyService.getCompanies(countryCode, taxIdentificationNumber);
    }
}


