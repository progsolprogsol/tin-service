package pl.progsol.tinservice.company;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import pl.progsol.tinservice.companyvies.CompanyVies;

@Mapper(componentModel = "spring")
public interface CompanyViesMapper {

    @Mappings({
            @Mapping(target = "countryCode", source = "country.code"),
            @Mapping(target = "countryName", source = "country.englishTranslation"),
            @Mapping(target = "companyTaxIdentificationNumber", source = "vatNumber")
    })
    Company companyViesToCompany(CompanyVies companyVies);
}
