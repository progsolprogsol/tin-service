package pl.progsol.tinservice.company;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import pl.progsol.tinservice.companygus.CompanyGus;

@Mapper(componentModel = "spring")
public interface CompanyGusMapper {

    @Mappings({
            @Mapping(target = "countryCode", constant = "PL"),
            @Mapping(target = "countryName", constant = "Poland"),
            @Mapping(target = "companyTaxIdentificationNumber", source = "nip"),
            @Mapping(target = "companyAddress", ignore = true)
    })
    Company companyGusToCompany(CompanyGus companyGus);

    @AfterMapping
    default void completeCompany(@MappingTarget Company company, CompanyGus companyGus) {
        String oneLineAddress = getOneLineAddress(companyGus);
        company.setCompanyAddress(oneLineAddress);
    }

    private String getOneLineAddress(CompanyGus companyGus) {
        String placePart = getPlacePart(companyGus.getPlace(), companyGus.getPlaceOfPostOffice());
        String streetPart = getStreetPart(companyGus.getStreet(), companyGus.getPropertyNumber(), companyGus.getApartmentNumber());
        String postOfficePart = getPostOfficePart(companyGus.getZipCode(), companyGus.getPlaceOfPostOffice());

        boolean placePartExist = StringUtils.isNotBlank(placePart);
        boolean streetPartExist = StringUtils.isNotBlank(streetPart);
        boolean postOfficePartExist = StringUtils.isNotBlank(postOfficePart);

        if (placePartExist && (streetPartExist || postOfficePartExist)) {
            placePart = placePart + "; ";
        }
        if (streetPartExist && postOfficePartExist) {
            streetPart = streetPart + "; ";
        }

        return placePart + streetPart + postOfficePart;
    }

    private String getStreetPart(String street, String propertyNumber, String apartmentNumber) {
        boolean streetExist = StringUtils.isNotBlank(street);
        boolean propertyNumberExist = StringUtils.isNotBlank(propertyNumber);
        boolean apartmentNumberExist = StringUtils.isNotBlank(apartmentNumber);

        String tmpStreet = street;
        String tmpPropNumber = propertyNumber;
        String tmpApartNumber = apartmentNumber;
        if (streetExist && (propertyNumberExist || apartmentNumberExist)) {
            tmpStreet = tmpStreet + " ";
        }
        if (propertyNumberExist && apartmentNumberExist) {
            tmpPropNumber = tmpPropNumber + " ";
        }
        if (apartmentNumberExist) {
            tmpApartNumber = "lok. " + tmpApartNumber;
        }
        return tmpStreet + tmpPropNumber + tmpApartNumber;
    }

    private String getPlacePart(String place, String placeOfPostOffice) {
        if (isPlaceMeaningful(place, placeOfPostOffice)) {
            return place;
        } else {
            return "";
        }
    }

    private String getPostOfficePart(String zipCode, String placeOfPostOffice) {
        boolean placeOfPostOfficeExist = StringUtils.isNotBlank(placeOfPostOffice);
        boolean zipCodeExist = StringUtils.isNotBlank(zipCode);

        String postOfficePart = "";
        if (zipCodeExist && placeOfPostOfficeExist) {
            postOfficePart = zipCode + " " + placeOfPostOffice;
        }
        return postOfficePart;
    }

    private boolean isPlaceMeaningful(String place, String placeOfPostOffice) {
        boolean placeExist = StringUtils.isNoneBlank(place);
        if (!placeExist) {
            return false;
        }
        boolean placeOfPostOfficeExist = StringUtils.isNoneBlank(placeOfPostOffice);
        if (!placeOfPostOfficeExist) {
            return true;
        }
        if (StringUtils.compareIgnoreCase(place, placeOfPostOffice) == 0) {
            return false;
        }
        return true;
    }
}
