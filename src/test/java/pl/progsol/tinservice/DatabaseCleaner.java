package pl.progsol.tinservice;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.progsol.tinservice.companygus.CompanyGusRepository;
import pl.progsol.tinservice.companygus.VoivodeshipRepository;
import pl.progsol.tinservice.companyvies.CompanyViesRepository;

@Component
@AllArgsConstructor
public class DatabaseCleaner {
    private final VoivodeshipRepository voivodeshipRepository;
    private final CompanyGusRepository companyGusRepository;
    private final CompanyViesRepository companyViesRepository;

    public void cleanupDb() {
        companyViesRepository.deleteAllInBatch();
        companyGusRepository.deleteAllInBatch();
        voivodeshipRepository.deleteAllInBatch();
    }
}
