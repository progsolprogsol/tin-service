package pl.progsol.tinservice.companyvies

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import pl.progsol.euapivies.ViesCountry
import pl.progsol.tinservice.DatabaseCleaner
import spock.lang.Specification
import spock.lang.Title

import java.time.Duration

@Title("Test 'CompanyViesService' business logic")
@SpringBootTest
// @SpringBootTest will use application defined datasource, so @AutoConfigureTestDatabase is not needed
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CompanyViesServiceTest extends Specification {

    @Value('${tin-service.maximum-date-age-in-millis}')
    private Long maxDateAgeInMillis;
    @Autowired
    private CompanyViesService companyViesService

    @Autowired
    private DatabaseCleaner databaseCleaner

    def cleanup() {
        databaseCleaner.cleanupDb()
    }

    def "test adding and refreshing a CompanyVies entity via service"() {
        given: "a country code and tax identification number (vat number)"
        def countryCode = ViesCountry.DENMARK.getCode()
        def vatNumber = "26033489"

        when: "getting a CompanyVies for the first time"
        CompanyVies companyVies = companyViesService.getCompanyVies(countryCode, vatNumber).get()

        then: "CompanyVies with appropriate data is returned"
        companyVies.getVatNumber() == vatNumber
        companyVies.getCompanyName() == "BRUUN RASMUSSEN HOLDING A/S"
        companyVies.getCompanyAddress() == "Sundkrogsgade 30; 2150 Nordhavn"
        companyVies.getCountry().getCode() == countryCode
        companyVies.getCountry().getName() == ViesCountry.DENMARK.toString()
        companyVies.getCountry().getPolishTranslation() == "Dania"
        companyVies.getCountry().getEnglishTranslation() == "Denmark"

        when: "wait till data is not actual and load again"
        sleep(maxDateAgeInMillis + 500)
        CompanyVies newCompanyVies = companyViesService.getCompanyVies(countryCode, vatNumber).get()

        then: "company data has been refreshed"
        Duration.between(companyVies.getLastRefresh(), newCompanyVies.getLastRefresh()).toMillis() > maxDateAgeInMillis
    }
}
