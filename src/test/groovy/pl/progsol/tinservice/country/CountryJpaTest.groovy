package pl.progsol.tinservice.country

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import pl.progsol.euapivies.ViesCountry
import spock.lang.Specification
import spock.lang.Title

import java.util.stream.Collectors

@Title("Test 'Country' JPA layer")
// Annotation for a JPA test that focuses only on JPA components.
// Using this annotation will disable full auto-configuration and instead apply only configuration relevant to JPA tests.
// By default, tests annotated with @DataJpaTest are transactional and roll back at the end of each test.
// They also use an embedded in-memory database (replacing any explicit or usually auto-configured DataSource).
// The @AutoConfigureTestDatabase annotation can be used to override these settings.
@DataJpaTest
// do not replace auto-configured DataSource (default behaviour of @DataJpaTest is to replace the main configuration)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CountryJpaTest extends Specification {
    @Autowired
    private CountryRepository countryDbRepository

    def "check if database contains all required countries"() {
        when: "querying all Countries"
        List<Country> countries = countryDbRepository.findAll()
        Map<String, Country> codeToCountry = countries.stream()
                .collect(Collectors.toMap(
                        { country -> ((Country) country).getCode() },
                        { country -> country })
                )

        then: "db Country collection contains all ViesCountry objects"
        countries.size() == ViesCountry.values().length
        for (ViesCountry viesCountry : ViesCountry.values()) {
            Country country = Objects.requireNonNull(codeToCountry.get(viesCountry.getCode()))
            viesCountry.getCode() == country.getCode()
            viesCountry.toString() == country.getName()
        }
    }
}
