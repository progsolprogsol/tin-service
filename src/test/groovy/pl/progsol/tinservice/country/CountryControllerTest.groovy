package pl.progsol.tinservice.country

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pl.progsol.euapivies.ViesCountry
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title
import spock.lang.Unroll

import java.nio.charset.StandardCharsets
import java.util.stream.Collectors

@Title("CountryController Specification")
@Narrative("""
The specification of the behaviour of the CountryController.
It can return information about allowed countries.
""")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CountryControllerTest extends Specification {
    @Autowired
    private MockMvc mvc
    @Autowired
    private ObjectMapper objectMapper

    private static TypeReference<List<CountryDto>> typeReferenceCountryDtoList

    def setupSpec() {
        typeReferenceCountryDtoList = new TypeReference<List<CountryDto>>() {}
    }

    def "when GET is performed then response has status 200 and appropriate content is returned"() {
        when: "requesting about all countries"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/countries"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        List<CountryDto> countryDtoList = objectMapper.readValue(responseAsString, typeReferenceCountryDtoList)

        then: "countries data is returned"
        List<ViesCountry> viesCountries = new ArrayList<>(Arrays.asList(ViesCountry.values())).stream()
                .sorted({ country1, country2 -> (country1.getCode() <=> country2.getCode()) })
                .collect(Collectors.toList())
        countryDtoList.size() == viesCountries.size()
        for (int i = 0; i < viesCountries.size(); i++) {
            ViesCountry viesCountry = viesCountries.get(i)
            CountryDto countryDto = countryDtoList.get(i)
            viesCountry.getCode() == countryDto.getCode()
        }
    }

    @Unroll
    def "when GET is performed then data for single country is returned and httStatus 200"() {
        given: "a country code"

        when: "requesting about single country"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/countries/$COUNTRY_CODE"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        CountryDto countryDto = objectMapper.readValue(responseAsString, CountryDto.class)

        then: "country data is returned"
        countryDto.getCode() == COUNTRY_CODE
        countryDto.getName() == countryName

        where:
        COUNTRY_CODE || countryName
        "PL"         || "Poland"
        "AT"         || "Austria"
        "BE"         || "Belgium"
        "BG"         || "Bulgaria"
        "CY"         || "Cyprus"
        "CZ"         || "Czech Republic"
    }
}
