package pl.progsol.tinservice.companygus

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import pl.progsol.tinservice.DatabaseCleaner
import spock.lang.Specification
import spock.lang.Title

import java.time.Duration

@Title("Test 'CompanyGusService' business logic")
@SpringBootTest
class CompanyGusServiceTest extends Specification {

    @Value('${tin-service.maximum-date-age-in-millis}')
    private Long maxDateAgeInMillis;
    @Autowired
    private CompanyGusService companyGusService

    @Autowired
    private DatabaseCleaner databaseCleaner

    def cleanup() {
        databaseCleaner.cleanupDb()
    }

    def "test of avoid of creating duplicated Voivodeships"() {
        given: "a two nips of companies from the same voivodeship"
        def nip1 = "7010363021"
        def nip2 = "5220017175"

        when: "getting companies from the same voivodeship"
        CompanyGus companyGus1 = companyGusService.getCompanyGusList(nip1).get(0)
        CompanyGus companyGus2 = companyGusService.getCompanyGusList(nip2).get(0)

        then: "voivodeship is the same object"
        companyGus1.getVoivodeship().getId() == companyGus2.getVoivodeship().getId()
        companyGus1.getVoivodeship().getName() == companyGus2.getVoivodeship().getName()
    }

    def "test adding and refreshing a CompanyGus entity via service"() {
        given: "a nip (polish equivalent of Tax Identification Number)"
        def nip = "7010363021"

        when: "getting a CompanyGus for the first time"
        List<CompanyGus> companyGusList = companyGusService.getCompanyGusList(nip)
        CompanyGus companyGus = companyGusList.get(0)

        then: "CompanyGus with appropriate data is returned"
        companyGusList.size() == 1
        companyGus.getRegon() == "146457539"
        companyGus.getNip() == "7010363021"
        companyGus.getNipState() == ""
        companyGus.getCompanyName() == "ACSB-WSPARCIE MAŁEGO BIZNESU Daniel Wieteska"
        companyGus.getVoivodeship().getName() == "MAZOWIECKIE"
        companyGus.getCounty() == "pruszkowski"
        companyGus.getCommune() == "Raszyn"
        companyGus.getPlace() == "Raszyn"
        companyGus.getZipCode() == "05-090"
        companyGus.getPlaceOfPostOffice() == "Raszyn"
        companyGus.getStreet() == "ul. Al. Krakowska"
        companyGus.getPropertyNumber() == "19"
        companyGus.getApartmentNumber() == ""
        companyGus.getCompanyType() == "F"
        companyGus.getSilosId() == 1
        companyGus.getBusinessTerminationDate() == null

        when: "wait till data is not actual and load again"
        sleep(maxDateAgeInMillis + 500)
        CompanyGus newCompanyGus = companyGusService.getCompanyGusList(nip).get(0)

        then: "company data has been refreshed"
        Duration.between(companyGus.getLastRefresh(), newCompanyGus.getLastRefresh()).toMillis() > maxDateAgeInMillis
    }
}



