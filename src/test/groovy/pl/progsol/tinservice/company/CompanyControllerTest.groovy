package pl.progsol.tinservice.company

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.servlet.NoHandlerFoundException
import pl.progsol.tinservice.DatabaseCleaner
import pl.progsol.tinservice.company.inputchecker.WrongCompanyInputDataException
import pl.progsol.tinservice.config.ErrorResponse
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title
import spock.lang.Unroll

import java.nio.charset.StandardCharsets

@Title("CompanyController Specification")
@Narrative("""
The specification of the behaviour of the CompanyController.
It can return information about any company in Europe.
""")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CompanyControllerTest extends Specification {
    @Autowired
    private MockMvc mvc
    @Autowired
    private ObjectMapper objectMapper

    private static TypeReference<List<Company>> typeReferenceCompanyList

    @Autowired
    private DatabaseCleaner databaseCleaner

    def setupSpec() {
        typeReferenceCompanyList = new TypeReference<List<Company>>() {}
    }

    def cleanup() {
        databaseCleaner.cleanupDb()
    }

    @Unroll
    def "when GET is performed then response has status 200 and appropriate content is returned"() {
        given: "a country code and company tax identification number"
        def COUNTRY_CODE = countryCode
        def TIN = taxIdentificationNumber

        when: "requesting about company"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/companies/$COUNTRY_CODE/$TIN"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        List<Company> companyList = objectMapper.readValue(responseAsString, typeReferenceCompanyList)

        then: "company data is returned"
        companyList.size() == 1
        Company company = companyList.get(0)
        company.getCountryCode() == COUNTRY_CODE
        company.getCompanyTaxIdentificationNumber() == TIN
        company.getCountryName() == countryName
        company.getCompanyName() == companyName
        company.getCompanyAddress() == companyAddress

        where:
        countryCode | taxIdentificationNumber || countryName     | companyName                                    | companyAddress
        "PL"        | "7010363021"            || "Poland"        | "ACSB-WSPARCIE MAŁEGO BIZNESU Daniel Wieteska" | "ul. Al. Krakowska 19; 05-090 Raszyn"
        "PL"        | "6920000013"            || "Poland"        | "KGHM POLSKA MIEDŹ SPÓŁKA AKCYJNA"             | "ul. Marii Skłodowskiej-Curie 48; 59-301 Lubin"
        "EL"        | "054934613"             || "Greece"        | "ΜΥΡΩΝΙΔΗΣ ΣΤΑΥΡΟΣ ΠΑΥΛΟΣ"                     | "Ν ΦΩΚΑ 8; 54621 - ΘΕΣΣΑΛΟΝΙΚΗ"
        "DK"        | "26033489"              || "Denmark"       | "BRUUN RASMUSSEN HOLDING A/S"                  | "Sundkrogsgade 30; 2150 Nordhavn"
        "GB"        | "169466070"             || "Grate Britain" | "FIRMA W ANGLII LIMITED"                       | "590 KINGSTON ROAD; LONDON; SW20 8DN"
        "FR"        | "58307048439"           || "France"        | "SA M & M MILITZER & MUNCH FRANCE"             | ".; AV DE MENIN; 59250 HALLUIN"
        "DE"        | "316163295"             || "Germany"       | "---"                                          | "---"
        "ES"        | "A58536111"             || "Spain"         | "---"                                          | "---"
    }

    def "test requesting about company which return two companies under one tin"() {
        given: "a tin (and country code) which returns two companies"
        def COUNTRY_CODE = "PL"
        def TIN = "1230501083"

        when: "requesting about company"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/companies/$COUNTRY_CODE/$TIN"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        List<Company> companyList = objectMapper.readValue(responseAsString, typeReferenceCompanyList)

        then: "two companies are returned"
        companyList.size() == 2
        Company company1 = companyList.get(0)
        Company company2 = companyList.get(1)
        company1.getCompanyName() == "GOSPODARSTWO HODOWLANE SPECJALISTYCZNE JAROSŁAW MUSIELAK"
        company1.getCompanyAddress() == "Niedabyl; 49; 26-804 Stromiec"
        company2.getCompanyName() == "GOSPODARSTWO ROLNE JAROSŁAW MUSIELAK"
        company2.getCompanyAddress() == "Korzeniówka; ul. Topolowa 23; 05-504 Złotokłos"
    }

    def "test requesting about company with letter in TIN"() {
        given: "a tin with letter"
        def countryCode = "IE"
        def tin = "6388047V"

        when: "requesting about company"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/companies/$countryCode/$tin"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        List<Company> companyList = objectMapper.readValue(responseAsString, typeReferenceCompanyList)

        then: "company is returned"
        companyList.size() == 1
        Company company = companyList.get(0)
        company.getCountryCode() == countryCode
        company.getCompanyTaxIdentificationNumber() == tin
        company.getCountryName() == "Ireland"
        company.getCompanyName() == "GOOGLE IRELAND LIMITED"
        company.getCompanyAddress() == "3RD FLOOR, GORDON HOUSE, BARROW STREET, DUBLIN 4"
    }

    @Unroll
    def "when GET with correct input is performed, but there is no data under such input, return 404 - Not Found"() {
        given: "a correct country code and TIN, but which do not exist"

        when: "requesting about company"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/companies/$COUNTRY_CODE/$TIN"))
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        ErrorResponse errorResponse = objectMapper.readValue(responseAsString, ErrorResponse.class)

        then: "return 404 and error message"
        mvcResult.getResponse().getStatus() == HttpStatus.NOT_FOUND.value()
        errorResponse.getException() == CompanyNotFoundException.class.getSimpleName()
        errorResponse.getMessage() == errorMessage

        where:
        COUNTRY_CODE | TIN           || errorMessage
        "PL"         | "70103630219" || "Company ($COUNTRY_CODE $TIN) not found"
        "DK"         | "0123456789"  || "Company ($COUNTRY_CODE $TIN) not found"
        "EL"         | "24"          || "Company ($COUNTRY_CODE $TIN) not found"
    }

    @Unroll
    def "when GET with wrong input, return appropriate HttpStatus and errorMessage"() {
        given: "a country code and TIN, but at least one of them is wrong"

        when: "requesting about company"
        MvcResult mvcResult = mvc
                .perform(MockMvcRequestBuilders.get("/companies/$COUNTRY_CODE/$TIN"))
                .andReturn()
        String responseAsString = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8)
        ErrorResponse errorResponse = objectMapper.readValue(responseAsString, ErrorResponse.class)

        then: "return appropriate HttpStatus and errorMessage"
        mvcResult.getResponse().getStatus() == ((HttpStatus) httpStatus).value()
        errorResponse.getException() == ((Class) errorClass).getSimpleName()
        errorResponse.getMessage() == errorMessage

        where:
        COUNTRY_CODE | TIN            || httpStatus                      | errorClass                           | errorMessage
        "PL"         | "012oo3456789" || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
        "EL"         | "123 456"      || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
        "DK"         | "abcDE()&^@"   || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
        "pl"         | "123456789"    || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Country Code ($COUNTRY_CODE) is incorrect, visit /countries endpoint to see allowed country codes"
        "125"        | "123456789"    || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Country Code ($COUNTRY_CODE) is incorrect, visit /countries endpoint to see allowed country codes"
        "!!!"        | "123456789"    || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Country Code ($COUNTRY_CODE) is incorrect, visit /countries endpoint to see allowed country codes"
        "%^&645"     | "abfRFT!@"     || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
        "PL"         | ""             || HttpStatus.NOT_FOUND            | NoHandlerFoundException.class        | "No handler found for GET /companies/$COUNTRY_CODE/$TIN"
        ""           | "7010363021"   || HttpStatus.NOT_FOUND            | NoHandlerFoundException.class        | "No handler found for GET /companies/$TIN"
        ""           | ""             || HttpStatus.NOT_FOUND            | NoHandlerFoundException.class        | "No handler found for GET /companies/"
        "   "        | "7010363021"   || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Country Code ($COUNTRY_CODE) is incorrect, visit /countries endpoint to see allowed country codes"
        "PL"         | "   "          || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
        "   "        | "   "          || HttpStatus.UNPROCESSABLE_ENTITY | WrongCompanyInputDataException.class | "Tax Identification Number ($TIN) is incorrect"
    }
}
