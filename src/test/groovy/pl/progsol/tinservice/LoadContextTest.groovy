package pl.progsol.tinservice

import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title

@Title("Application Specification")
@Narrative("Specification which beans are expected")
@SpringBootTest
//@EnableAutoConfiguration(exclude = [
//        DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class,
//        HibernateJpaAutoConfiguration.class
//])
class LoadContextTest extends Specification {
    def "when context is loaded then all expected beans are created"() {
        expect: "application context is created"
    }
}
