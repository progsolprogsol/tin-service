#!/usr/bin/env bash
docker container stop tin-service
docker container rm tin-service
docker image rm tin-service
docker image pull docker.progsol.pl/tin-service:1.0
docker-compose --file /opt/tin-service/app/docker-compose-app.yml up --detach
