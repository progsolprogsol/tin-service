#!/usr/bin/env bash
###
#
if [ ${#} -eq 0 ]; then
    echo "No gradle wrapper version defined, exiting script..."
    exit
fi
# the $# variable will tell you the number of input arguments the script was passed
#
###
#
gradlePathWithShFilename="${BASH_SOURCE[0]}"
# example: /mnt/projects/gradle/000-set-gradle-wrapper-version.sh
# or
# example: ./gradle/000-set-gradle-wrapper-version.sh
#
###
#
onlyGradlePath=$(dirname "${gradlePathWithShFilename}")
# example: /mnt/projects/gradle
# or
# example: ./gradle
#
###
#
fullGradlePath=$(cd "${onlyGradlePath}" && pwd)
# here we will always obtain full path, so we eventualy trunsform relative paths
# for example: ./gradle to /mnt/projects/gradle
# we DO NOT change directory
#
###
#
projectFullPath=$(dirname "${fullGradlePath}")
# get parent directory
#
###
#
(cd "${projectFullPath}" && ./gradlew wrapper --warning-mode=all --gradle-version="${1}")
# example: ./gradlew wrapper --warning-mode=all --gradle-version=6.5
# we DO NOT change directory thanks to the brackets
#
###