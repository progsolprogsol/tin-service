#!/usr/bin/env bash
imageVersion="1.0"
gradle clean test bootJar --settings-file=../../settings.gradle
docker image build --tag progsol.pl/tin-service:${imageVersion} ../..
docker push docker.progsol.pl/tin-service:${imageVersion}
#
